package com.example.appcalculadora

class Calculadora(var num1: Float, var num2: Float) {

    fun suma(): Float {
        return num1 + num2
    }

    fun resta(): Float {
        return num1 - num2
    }

    fun multiplicacion(): Float {
        return num1 * num2
    }

    fun division(): Float {
        return if (num2 != 0.0f) {
            num1 / num2
        } else {
            Float.NaN
        }
    }
}