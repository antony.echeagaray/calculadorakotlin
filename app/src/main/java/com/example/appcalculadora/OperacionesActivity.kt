package com.example.appcalculadora

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {

    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var txtResultado: TextView
    private lateinit var txtUsuario: TextView
    private lateinit var btnSuma: Button
    private lateinit var btnResta: Button
    private lateinit var btnMultiplicacion: Button
    private lateinit var btnDivision: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operaciones)

        // Inicializa las vistas
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        txtResultado = findViewById(R.id.txtResultado)
        txtUsuario = findViewById(R.id.txtUsuario)
        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)
        btnMultiplicacion = findViewById(R.id.btnMultiplicacion)
        btnDivision = findViewById(R.id.btnDivision)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnSalir = findViewById(R.id.btnSalir)

        // Recibe el nombre de usuario desde el Intent
        val nombre = intent.getStringExtra("usuario")
        txtUsuario.text = "Usuario: $nombre"

        // Configura los botones de operación
        btnSuma.setOnClickListener {
            realizarOperacion("suma")
        }

        btnResta.setOnClickListener {
            realizarOperacion("resta")
        }

        btnMultiplicacion.setOnClickListener {
            realizarOperacion("multiplicacion")
        }

        btnDivision.setOnClickListener {
            realizarOperacion("division")
        }

        // Configura el botón de limpiar
        btnLimpiar.setOnClickListener {
            limpiarCampos()
        }

        // Configura el botón de salir
        btnSalir.setOnClickListener {
            finish()
        }
    }

    private fun realizarOperacion(operacion: String) {
        val num1 = txtNum1.text.toString().toFloatOrNull()
        val num2 = txtNum2.text.toString().toFloatOrNull()

        if (num1 != null && num2 != null) {
            val calculadora = Calculadora(num1, num2)
            val resultado: Float = when (operacion) {
                "suma" -> calculadora.suma()
                "resta" -> calculadora.resta()
                "multiplicacion" -> calculadora.multiplicacion()
                "division" -> {
                    if (num2 == 0.0f) {
                        mostrarDialogo("Error", "No se puede dividir entre cero")
                        return
                    } else {
                        calculadora.division()
                    }
                }
                else -> 0.0f
            }
            txtResultado.text = resultado.toString()
        } else {
            mostrarDialogo("Error", "Entrada no válida")
        }
    }

    private fun mostrarDialogo(titulo: String, mensaje: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun limpiarCampos() {
        txtNum1.text.clear()
        txtNum2.text.clear()
        txtResultado.text = ""
    }
}